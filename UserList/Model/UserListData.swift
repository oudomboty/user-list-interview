//
//  UserListData.swift
//  UserList
//
//  Created by Boty Thieng on 5/1/23.
//

import Foundation

struct UserListData: Codable {
    var data: [DetailData]
}

struct DetailData: Codable {
    let id: String
    let age: Int
    let name: String
    let type: String
    let title: String
    let avatar: String
}

