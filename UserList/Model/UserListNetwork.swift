//
//  UserListNetwork.swift
//  UserList
//
//  Created by Boty Thieng on 5/1/23.
//

import Foundation

protocol UserListNetworkDelegate {
    func didUpdatedUser(user: [DetailData])
    func didFailWithError(error: Error)
}

struct UserListNetwork {
    
    let urlString = "https://api.npoint.io/2f788b4e69cb22c5e2ec"
    var delegate: UserListNetworkDelegate?
    
    func performRequest() {
        if let url = URL(string: urlString) {
            let session = URLSession(configuration: .default)
            let task = session.dataTask(with: url) { data, response, error in
                if error != nil {
                    print(error!)
                } else if let safeData = data, let responseExist = response as? HTTPURLResponse {
                    if responseExist.statusCode == 200 {
//                        print(String.init(data: safeData, encoding: .ascii)!)
//                        print(safeData)
                        let userList = self.parseJson(User: safeData)
                            self.delegate?.didUpdatedUser(user: userList)
                    } else {
                        print(error!)
                    }
                }
            }
            task.resume()
        }
    }
    
    func parseJson(User: Data) ->  [DetailData]{
        var user = [DetailData]()
        let decoder = JSONDecoder()
        do {
            let decodeData = try decoder.decode(UserListData.self, from: User)
            decodeData.data.forEach { datas in
                
                let model = DetailData(id: datas.id,
                                       age: datas.age,
                                       name: datas.name,
                                       type: datas.type,
                                       title: datas.title,
                                       avatar: datas.avatar)
                user.append(model)
            }
            return user
        } catch {
            delegate?.didFailWithError(error: error)
            return []
        }
    }
}
