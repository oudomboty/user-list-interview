//
//  AppDelegate.swift
//  UserList
//
//  Created by Boty Thieng on 5/1/23.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        loginView()
        return true
    }

    private func loginView() {
        let loginView = ViewController.instantiate()
        let navController  = UINavigationController(rootViewController: loginView)
        window?.rootViewController = navController
        window?.makeKeyAndVisible()
    }


}

