//
//  UserListTableViewCell.swift
//  UserList
//
//  Created by Boty Thieng on 5/1/23.
//

import UIKit

protocol UserListTableViewCellProtocol {
    func cellViewType(type: String)
}

class UserListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var rectangleView: UIView!{
        didSet {
//            rectangleView.backgroundColor = .white
            rectangleView.layer.cornerRadius = 10.0
            rectangleView.layer.shadowColor = UIColor.gray.cgColor
            rectangleView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
            rectangleView.layer.shadowRadius = 1.0
            rectangleView.layer.shadowOpacity = 0.7
        }
    }
    @IBOutlet weak var rowView: UIView!{
        didSet {
//            rowView.backgroundColor = .white
            rowView.layer.cornerRadius = 10.0
            rowView.layer.shadowColor = UIColor.gray.cgColor
            rowView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
            rowView.layer.shadowRadius = 1.0
            rowView.layer.shadowOpacity = 0.7
        }
    }
    
    @IBOutlet weak var rectangleUserimg: UIImageView!
    @IBOutlet weak var rowUserImg: UIImageView!
    
    @IBOutlet weak var rectangleNameLb: UILabel!
    @IBOutlet weak var rowNameLb: UILabel!
    
    @IBOutlet weak var rectangleTitleAgeLb: UILabel!
    @IBOutlet weak var rowTitleAgeLb: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func updatedCell(entity: DetailData) {
        if entity.type == "rectangle" {
            rowView.isHidden = true
            rectangleView.isHidden = false
            rectangleNameLb.text = entity.name
            rectangleTitleAgeLb.text = "\(entity.title) - \(entity.age)"
            if let url = URL(string: entity.avatar) {
                DispatchQueue.main.async {
                    let data = try? Data(contentsOf: url)
                    if let imageData = data {
                        self.rectangleUserimg.image = UIImage(data: imageData)
                    }
                }
            }
        } else {
            rectangleView.isHidden = true
            rowView.isHidden = false
            rowNameLb.text = entity.name
            rowTitleAgeLb.text = "\(entity.title) - \(entity.age)"
            if let url = URL(string: entity.avatar) {
                DispatchQueue.main.async {
                    let data = try? Data(contentsOf: url)
                    if let imageData = data {
                        self.rowUserImg.image = UIImage(data: imageData)
                    }
                }
            }
             

            
        }
    }
    
}
