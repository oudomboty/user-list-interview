//
//  ViewController.swift
//  UserList
//
//  Created by Boty Thieng on 5/1/23.
//

import UIKit

class ViewController: UIViewController {
    
    static func instantiate() -> ViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! ViewController
        return controller
    }
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.dataSource = self
            tableView.delegate = self
        }
    }
    
    var userListNetwork = UserListNetwork()
    var userListData = [DetailData]()
    let type: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "User Lists"
//        tableView.backgroundColor = .gray
        userListNetwork.delegate = self
        userListNetwork.performRequest()
        let nib = UINib(nibName: "UserListTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "UserListTableViewCell")

    }


}
extension ViewController: UserListNetworkDelegate {
    func didUpdatedUser(user: [DetailData]) {
        DispatchQueue.main.async {
            self.userListData = user
            
            self.tableView.reloadData()
            
        }
    }
    
    func didFailWithError(error: Error) {
        print(error)
    }
}



extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        let entity = userListData[indexPath.row]
        if entity.type == "rectangle" {
            return 200
        } else {
            return 120
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userListData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserListTableViewCell", for: indexPath) as! UserListTableViewCell
        let entity = userListData[indexPath.row]
        cell.rectangleView.isHidden = true
        cell.updatedCell(entity: entity)
        return cell
    }
    
    
}

